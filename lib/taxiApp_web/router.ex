defmodule TaxiAppWeb.Router do
  use TaxiAppWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", TaxiAppWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", BookingsController, :index)
    get("/myTravels", PageController, :myTravels)
    get("/csrClient", PageController, :csrClient)
    get("/csrOperator", PageController, :csrOperator)
    get("/oLostCSR", PageController, :oLostCSR)
    get("/oScamCSR", PageController, :oScamCSR)
    get("/driver", DriverController, :index)
    get("/dMyTravels", PageController, :dMyTravels)
    get("/applyDriver", PageController, :applyDriver)

    #auth
    get("/login", PageController, :login)
    get("/logout", PageController, :logout)
    get("/register", PageController, :register)
  end

  # Other scopes may use custom stacks.
  scope "/api", TaxiAppWeb do
    pipe_through(:api)

    get("/bookings/status/:id", Api.BookingsController, :getBookingsStatus)
    get("/bookings", Api.BookingsController, :get)
    get("/user", Api.UserController, :get)
    get("/bookings/user", Api.BookingsController, :getBookingsByUser)
    get("/bookings/accepted/:driverId", Api.BookingsController, :getAcceptedBookings)
    get("/bookings/driver", Api.BookingsController, :getBookingsByDriver)
    get("/bookings/:driverId", Api.BookingsController, :getOpenBookingsByDriver)
    get("/driver/available", Api.DriverController, :availableDrivers)
    post("/bookings/new", Api.BookingsController, :create)
    post("/csr/new", Api.CsrController, :create)
    get("/csr", Api.CsrController, :get)
    post("/csr/update/:id", Api.CsrController, :update)
    post("/driver/update/:id", Api.DriverController, :update)
    post("/bookings/update/:id", Api.BookingsController, :updateStatus)

    #Auth
    post("/auth/new", Api.AuthController, :create)
    post("/auth/login", Api.AuthController, :login)
  end
end
