defmodule TaxiAppWeb.Api.DriverController do
    use TaxiAppWeb, :controller
    import Ecto.Query, only: [from: 2]
    alias Ecto.Changeset
    alias TaxiApp.{User.Driver, Repo}

    def update(conn, %{"id" => id, "location" => loc, "status" => status}) do

        driver = Repo.get!(Driver, id)
        driver = Changeset.change driver, %{status: status, location: loc}
        
        case Repo.update driver do
            {:ok, _struct} -> put_status(conn, 200) |> json(%{msg: "Updated driver"})
            {:error, _changeset} -> put_status(conn, 400) |> json(%{msg: "Failed to update driver", error: :error})
        end
    end

    def availableDrivers(conn, _params) do
        query = from t in Driver, 
                where: t.status == "available", 
                select: t

        available_drivers = Repo.all(query)
        
        put_status(conn, 200) |>  json(%{drivers: available_drivers})
    end
end