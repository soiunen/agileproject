defmodule TaxiAppWeb.Api.BookingsController do
  use TaxiAppWeb, :controller
  import Ecto.Query
  alias Ecto.Changeset
  alias TaxiApp.{Booking, Repo, View.BookingDriverInfo}

  def get(conn, params) do
   
    {:ok, date} = Date.from_iso8601(params["date"])

    q = from p in BookingDriverInfo,
        where: fragment("date(inserted_at) = ?", ^date),
        select: p

    result = Repo.all q

    put_status(conn, 200) |> json(%{info: result})
  end

  def getAcceptedBookings(conn,  %{"driverId" => driverId}) do
    query = from t in Booking, 
            where: t.driverId == ^driverId and t.status == "accepted", 
            select: t
    
    result = Repo.one(query)
    
    put_status(conn, 200) |> json(%{bookings: result})
  end

  def updateStatus(conn, %{"id" => id, "status" => status}) do
    booking = Repo.get!(Booking, id)
    booking = Changeset.change booking, %{status: status}
        
    case Repo.update booking do
        {:ok, _struct} -> put_status(conn, 200) |> json(%{msg: "Updated booking", status: status})
        {:error, _changeset} -> put_status(conn, 400) |> json(%{msg: "Failed to update booking", error: :error})
    end
  end

  def getOpenBookingsByDriver(conn, %{"driverId" => driverId}) do
     
    query = from t in Booking, 
            where: t.driverId == ^(String.to_integer(driverId)) and t.status == "open", 
            select: t
    
    result = Repo.all(query)
    
    put_status(conn, 200) |> json(%{bookings: result})
  end

  def getBookingsByDriver(conn, _params) do
    userId = fetch_session(conn) |> get_session(:id)

    query = from t in BookingDriverInfo, 
            where: t.userid == ^userId and t.status == "completed", 
            select: t
    
    result = Repo.all(query)
    
    put_status(conn, 200) |> json(%{bookings: result})
  end


  def getBookingsByUser(conn, _params) do
    userId = fetch_session(conn) |> get_session(:id)
    query = from t in Booking, 
            where: t.clientId == ^userId, 
            select: t
    
    result = Repo.all(query)
    
    put_status(conn, 200) |> json(%{bookings: result})
  end

  def getBookingsStatus(conn, %{"id" => id}) do
    query = from t in Booking, 
            where: t.id == ^(String.to_integer(id)), 
            select: t.status
    
    result = Repo.one(query)
    
    put_status(conn, 200) |> json(%{bookings: result})
  end

  def create(conn, params) do
    clientId = fetch_session(conn) |> get_session(:id)
    booking_details = params["bookings"]

    booking_params = %{
      dropoff: booking_details["dropoff"],
      pickup: booking_details["pickup"],
      bookingData: booking_details["bookingData"],
      cost: booking_details["cost"],
      distance: booking_details["distance"],
      driverId: booking_details["driverId"],
      clientId: clientId,
      feedback: "Very nice",
      status: booking_details["status"]
    }
    
    booking_changeset = Booking.changeset(%Booking{}, booking_params)
   
    insertresp = Repo.insert!(booking_changeset)

    put_status(conn, 201) |> json(%{msg: "Booking created", id: insertresp.id})
  end

end
