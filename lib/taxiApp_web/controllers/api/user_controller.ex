defmodule TaxiAppWeb.Api.UserController do
    use TaxiAppWeb, :controller
    import Ecto.Query
    alias Ecto.Changeset
    alias TaxiApp.{Repo, View.UserDriver}

    def get(conn, _params) do
        userId = fetch_session(conn) |> get_session(:id)
    
        query = from t in UserDriver, 
                where: t.userid == ^userId, 
                select: t
        
        result = Repo.one(query)
        
        put_status(conn, 200) |> json(%{user: result})
      end
  

end