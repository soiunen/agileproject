defmodule TaxiAppWeb.Api.CsrController do
    use TaxiAppWeb, :controller
    import Ecto.Query
    alias Ecto.Changeset
    alias TaxiApp.{Repo, Reports.CSR}

    
    def create(conn, params) do
      
      csr_params = params["csr"]

      csr_changeset = CSR.changeset(%CSR{}, csr_params)

      case Repo.insert csr_changeset do
        {:ok, _struct} -> put_status(conn, 200) |> json(%{msg: "CSR report created"})
        {:error, _changeset} -> put_status(conn, 400) |> json(%{msg: "Failed to create a CSR report"})
      end
    end

    def update(conn, %{"id" => id, "status" => status}) do

      csr = Repo.get!(CSR, id)
      csr = Changeset.change csr, %{status: status}
      
      case Repo.update csr do
          {:ok, _struct} -> put_status(conn, 200) |> json(%{msg: "Updated CSReport status"})
          {:error, _changeset} -> put_status(conn, 400) |> json(%{msg: "Failed to update CSReport status", error: :error})
      end
    end


    def get(conn, params) do

      filters = Changeset.cast(%CSR{}, params, [:status, :type, :date, :pickup, :dropoff]) |> Map.fetch!(:changes) |> Map.to_list
      
      result = CSR |> order_by([desc: :inserted_at]) |> where(^filters) |> Repo.all

      put_status(conn, 200) |> json(%{reports: result})
    end 

end