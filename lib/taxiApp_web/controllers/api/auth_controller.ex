defmodule TaxiAppWeb.Api.AuthController do
  use TaxiAppWeb, :controller
  import Ecto.Query
  alias Ecto.Changeset
  alias TaxiApp.{User, Repo, User.User}


  def login(conn, params) do
    login_details=params["user"]
    query = from u in User, 
            where: u.email == ^login_details["email"] and u.password==^login_details["password"], 
            select: u
    
    result = Repo.one(query)
    
    if result == nil do
      put_status(conn, 200) |> json(%{msg: "User not found"})
    else#|> redirect(to: "/")
      # fetch_session(conn) |> put_session(:message, "new stuff we just set in the session")
      # |> put_status(conn, 200) |> json(%{login: result, msg: "Logged in"})
      fetch_session(conn) |> put_session(:id,  result.id)  |> put_session(:name,  result.name) |> put_session(:role,  result.role) |> put_status(200) |> json(%{login: result, msg: "Logged in"})
      
    end
    
  end

  

  def create(conn, params) do
      
    user_details = params["user"]

    booking_params = %{
      name: user_details["name"],
      password: user_details["password"],
      email: user_details["email"],
      role: user_details["role"],
      balance: 0,
    }
    #adds check if user exists
    user_changeset = User.changeset(%User{}, booking_params)
    if user_exist_by_mail(booking_params.email) do
      put_status(conn, 201) |> json(%{msg: "User exists", email: booking_params.email})
    else
      insertresp = Repo.insert!(user_changeset)
      put_status(conn, 201) |> json(%{msg: "User created", id: insertresp.id, mail: insertresp.email})
    end
  end


  defp user_exist_by_mail(email)do
    #select just id to reduce payload
    #query = (from u in User, where: u.email == ^email, select: %{email: u.email})
    query = (from u in User, where: u.email == ^email, select: u)
    found=Repo.one(query)
    #if not result, found will be nil
    found != nil
end
end