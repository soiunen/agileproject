defmodule TaxiAppWeb.DriverController do
    use TaxiAppWeb, :controller
  
    def index(conn, _params) do
      render(conn, "index.html")
    end
  end
  