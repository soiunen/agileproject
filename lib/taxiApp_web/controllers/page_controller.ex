defmodule TaxiAppWeb.PageController do
  use TaxiAppWeb, :controller

  def register(conn, _params) do
    render(conn, "register.html")
  end

  def login(conn, _params) do
    render(conn, "login.html")
  end

  def logout(conn, params) do
    clear_session(conn) |> redirect(to: "/")
  end

  def scamCSR(conn, _params) do
    render(conn, "scamCSR.html")
  end

  def csrOperator(conn, _params) do
    render(conn, "csrOperator.html")
  end

  def csrClient(conn, _params) do
    render(conn, "csrClient.html")
  end

  def oScamCSR(conn, _params) do
    render(conn, "oScamCSR.html")
  end

  def myTravels(conn, _params) do
    render(conn, "myTravels.html")
  end

  def dMyTravels(conn, _params) do
    render(conn, "dMyTravels.html")
  end
  def driver(conn, _params) do
    render(conn, "driver.html")
  end

  def applyDriver(conn, _params) do
    render(conn, "applyDriver.html")
  end
end
