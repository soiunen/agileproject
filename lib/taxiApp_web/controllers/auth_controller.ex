defmodule TaxiAppWeb.AuthController do
  use TaxiAppWeb, :controller

  def login(conn, _params) do
    render(conn, "login.html")
  end
  def register(conn, _params) do
    render(conn, "register.html")
  end
  def logout(conn, _params) do
    #destroy session
    #render(conn, "login.html")
  end
end