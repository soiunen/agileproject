defmodule TaxiApp.User.User do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :balance, :email, :name, :password, :role]}

  schema "users" do
    field :balance, :float
    field :email, :string
    field :name, :string
    field :password, :string
    field :role, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :password, :email, :role, :balance])
    |> validate_required([:name, :password, :email, :role, :balance])
  end
end
