defmodule TaxiApp.User.Driver do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :earnings, :status, :user_id, :location]}

  schema "drivers" do
    field :earnings, :float
    field :status, :string
    field :user_id, :id
    field :location, :string

    timestamps()
  end

  @doc false
  def changeset(driver, attrs) do
    driver
    |> cast(attrs, [:status, :earnings, :location])
    |> validate_required([:status, :earnings, :location])
  end
end
