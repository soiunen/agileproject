defmodule TaxiApp.View.UserDriver do
    use Ecto.Schema

    @derive {Poison.Encoder, only: [:id, :userid, :status, :location]}


    @primary_key false
    schema "v_user_driver" do

      field(:id, :integer)
      field(:userid, :integer)
      field(:status, :string)
      field(:location, :string)

    end
  end