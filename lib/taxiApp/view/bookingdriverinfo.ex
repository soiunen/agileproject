defmodule TaxiApp.View.BookingDriverInfo do
    use Ecto.Schema

    @derive {Poison.Encoder, only: [:id, :userid, :email, :status, :name, :bookingData, :cost, :distance, :dropoff, :feedback, :pickup, :inserted_at, :driverstatus]}


    @primary_key false
    schema "v_booking_driver_info" do

      field(:id, :integer)
      field(:userid, :integer)
      field(:status, :string)
      field(:email, :string)
      field(:name, :string)
      field(:bookingData, :string)
      field(:cost, :float)
      field(:distance, :float)
      field(:dropoff, :string)
      field(:feedback, :string)
      field(:pickup, :string)
      field(:inserted_at, :naive_datetime)
      field(:driverstatus, :string)

    end
  end