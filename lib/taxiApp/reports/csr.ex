defmodule TaxiApp.Reports.CSR do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :date, :status, :description, :dropoff, :email, :pickup, :title, :type]}


  schema "csrreports" do
    field :date, :date
    field :status, :string
    field :description, :string
    field :dropoff, :string
    field :email, :string
    field :pickup, :string
    field :title, :string
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(csr, attrs) do
    csr
    |> cast(attrs, [:type, :title, :description, :dropoff, :pickup, :date, :email, :status])
    |> validate_required([:type, :title, :description, :dropoff, :pickup, :date, :email, :status])
  end
end
