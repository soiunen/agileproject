defmodule TaxiApp.Booking do
  use Ecto.Schema
  import Ecto.Changeset
  
  @derive {Poison.Encoder, only: [:id, :bookingData, :clientId, :cost, :distance, :driverId, :dropoff, :feedback, :pickup, :status, :inserted_at]}

  schema "bookings" do
    field(:bookingData, :string)
    field(:clientId, :integer)
    field(:cost, :float)
    field(:distance, :float)
    field(:driverId, :integer)
    field(:dropoff, :string)
    field(:feedback, :string)
    field(:pickup, :string)
    field(:status, :string)

    timestamps()
  end

  @doc false
  def changeset(booking, attrs) do
    booking
    |> cast(attrs, [
      :pickup,
      :dropoff,
      :driverId,
      :clientId,
      :bookingData,
      :cost,
      :distance,
      :feedback,
      :status
    ])
    |> validate_required([
      :pickup,
      :dropoff,
      :driverId,
      :clientId,
      :bookingData,
      :cost,
      :distance,
      :feedback,
      :status
    ])
  end
end
