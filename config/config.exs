# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :taxiApp,
  ecto_repos: [TaxiApp.Repo]

# Configures the endpoint
config :taxiApp, TaxiAppWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8HmDaaZv02a2zRG4eaHwsPU8/PRvNGd/57JkM/wxZgkvL8F7l2treyS0y33zHlzA",
  render_errors: [view: TaxiAppWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TaxiApp.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
