# TaxiApp

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install && cd ..`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

To use the UI fill in the form and click "Submit". If the form is not filled, error message will appear. Next the application will display distance, average cost and travel time for the trip. If the user is happy with the price he/she should click "Order" which will display time until arrival.

