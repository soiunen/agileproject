defmodule TaxiApp.Repo.Migrations.CreateDrivers do
  use Ecto.Migration

  def change do
    create table(:drivers) do
      add :status, :string
      add :earnings, :float
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:drivers, [:user_id])
  end
end
