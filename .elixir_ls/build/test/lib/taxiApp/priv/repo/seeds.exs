# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     TaxiApp.Repo.insert!(%TaxiApp.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

import Ecto.Query, only: [from: 2]
alias TaxiApp.{Repo, User.User, User.Driver, Reports.CSR}

Repo.insert!(%CSR{date: Ecto.Date.cast!("2018-12-03"), status: "open", email: "john.doe@mail.com", description: "Test", dropoff: "Liivi 2", pickup: "Pikk 2", title: "testtitle", type: "lost"})

Repo.insert!(%User{name: "Teemu Selänne", password: "pass", email: "selanne@mail.com", role: "user", balance: 100.0})

Repo.insert!(%User{name: "Robert Smith", password: "pass", email: "rsmith@mail.com", role: "operator", balance: 0.0})

Repo.insert!(%User{name: "John Doe", password: "pass", email: "john.doe@mail.com", role: "driver", balance: 0.0})
Repo.insert!(%Driver{status: "invisible", earnings: 0.0, location: "Eeden Tartu", user_id: Repo.one(from u in User, where: u.name == "John Doe", select: u.id)})

Repo.insert!(%User{name: "Jane Doe", password: "pass", email: "jane.doe@mail.com", role: "driver", balance: 0.0})
Repo.insert!(%Driver{status: "invisible", earnings: 0.0, location: "Tartu Vangla", user_id: Repo.one(from u in User, where: u.name == "Jane Doe", select: u.id)})