var map;
var markers = [];
const geocoder = new google.maps.Geocoder;
const directionsService = new google.maps.DirectionsService;
const directionsDisplay = new google.maps.DirectionsRenderer;
const distanceService = new google.maps.DistanceMatrixService;

//create map
export function createMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: { lat: 58.3717071, lng: 26.759830700000066 }
    });
    return map
}

//function for mounted map
export function getCurrentLocation() {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(position => {
            const loc = { lat: position.coords.latitude, lng: position.coords.longitude };
            geocoder.geocode({
                location: loc
            }, function (response, status) {
                if (status === "OK" && response[0]) {
                    if (map) {
                        map.setCenter(loc);
                        map.setZoom(15);
                    }
                    markers.push(new google.maps.Marker({ position: loc, map, title: "Pickup address" }));
                    const current_location = response[0].formatted_address
                    resolve({ current_location });
                }
                else {
                    reject('Current location request failed due to ' + status)
                }
            });
        })
    });
}
//function to get cost, distance and travel time
export function getRoute(pickup_address, dropoff_address, points=[]) {
    return new Promise((resolve, reject) => {
        directionsDisplay.setMap(map);
        directionsService.route({
            origin: pickup_address,
            waypoints: points,
            destination: dropoff_address,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                clearMarkers();
                const { distance, duration } = response.routes[0].legs[0]
                directionsDisplay.setDirections(response);
                let km = distance.value;
                const distanceInMeters = distance.value / 1000;
                const travelTime = duration.text;
                const cost = ((km / 1000) * 0.8 + 3).toFixed(2);

                resolve({ cost, distance: distanceInMeters, travelTime })
            } else {
                reject('Directions request failed due to ' + status);
            }
        });
    })
}

export function getAllDistance(driver_locations, pickup_address) {
    return new Promise((resolve, reject) => {
        distanceService.getDistanceMatrix(
            {
              origins: driver_locations,
              destinations: [pickup_address],
              travelMode: 'DRIVING'
            }, function (response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    const data = response.rows.map(ele => ele.elements[0])
                    resolve(data)
                } else {
                    reject('Directions request failed due to ' + status);
                }
            });
    })
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearMarkers() {
    setMapOnAll(null);
}
