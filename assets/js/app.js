// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
//import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"
import Vue from "vue";
import axios from "axios";
import "./socket"; // To be used later

//importing 
import { getRoute, getCurLoc, createMap, getCurrentLocation, getAllDistance } from './apicall';

//import customer from "./customer";
//Vue.component("homeComponent");

//new Vue({}).$mount("#home");

//vue for login
if ( window.location.pathname.startsWith("/login")){
  new Vue({
    el: "#login",
    data: {    
      password: "",
      role: "",
      email: "",
      name: "",
      balance: 0
    },
    methods: {    
      login: function () {
        if (this.email=== "" ||  this.password=== "" ) 
        {
          alert("Please fill in the form!");
        }     
        else {
          this.status = "submitted"
          //api call to save to db
          axios.post("/api/auth/login", {
            user: {
              email: this.email,
              password: this.password,
            }
          })
            .then(response => {
              console.log(response);
              if (response.data.msg==="Logged in") {
                window.location.href = "/";
              }
              else{
                alert(response.data.msg)
              }    
            })
        }

      }
    },
    mounted: function () { }
  });
}

//vue for register
if (window.location.pathname.startsWith("/register")){
  new Vue({
    el: "#register",
    data: {    
      registered: 0,
      role: "",
      email: "",
      name: "",
      password: "",
      password2:"",
      balance: 0
    },
    methods: {    
      register: function () {
        if (this.email=== "" ||  this.name=== "" ||  this.password=== "" ) 
        {
          alert("Please fill in the form!");
        }
        else if(this.password !== this.password2){
          alert("Passwords do not match!");
        }        
        else {
          this.status = "submitted"
          //api call to save to db
          axios.post("/api/auth/new", {
            user: {
              role: "user",
              email: this.email,
              name: this.name,
              password: this.password,
              balance: 0
            }
          })
            .then(response => {
              console.log(response);
              if (response.data.msg==="User created") {
                this.registered=1;
              }
              else{
                alert(response.data.msg)
              }    
            })
        }

      }
    },
    mounted: function () { }
  });
}  

if (window.location.pathname === "/"){
  new Vue({
    el: "#home",
    data: {
      phone: "",
      name: "",
      pickup: "",
      dropoff: "",
      cost: 0,
      distance: 0,
      status: 0,
      travelTime: 0,
      arrivalTime: "",
      id: 0,
      interval: null,
      previousDelay: 0,
      driverMessage: "",
      chosen_driver_location: ""
    },
    methods: {
      orderTaxi: function () {
        this.status = 1;
        let closest = 999;
        let chosen_driver = null;
        
        axios.get("/api/driver/available")
          .then(response => {
            const drivers = response["data"]["drivers"];
            
            if (drivers.length < 1) {
              this.status = 5
              return
            }

            const locs = drivers.map(el => el.location)
        
            let distances = getAllDistance(locs, this.pickup)

            distances.then(data =>{
              drivers.map( (driver,i) => {
                driver["duration"] = data[i].duration
                driver["distance"] = data[i].distance 
              })

              drivers.sort((a,b) => a.duration.value - b.duration.value)
              const chosen_driver = drivers[0]
              this.arrivalTime = chosen_driver.duration.text
              this.chosen_driver_location = chosen_driver.location
              console.log(this.chosen_driver)

              axios.post("/api/bookings/new", {
                bookings: {
                  pickup: this.pickup,
                  dropoff: this.dropoff,
                  cost: this.cost,
                  distance: this.distance,
                  bookingData: this.name + "|" + this.phone,
                  driverId: chosen_driver.id,
                  status: "open"
                }
              })
                .then(response => {
                  console.log(response)
                  this.id = response.data.id
                  this.interval = window.setInterval(this.checkBookingStatus, 10000)

                })
            })
          })
          .catch(error => console.log(error));
      },

      checkBookingStatus: function () {
        axios
          .get("/api/bookings/status/" + this.id)
          .then(response => {
            const bookingStatus = response.data.bookings;
            if (bookingStatus == "open") {
              this.status = 1;
            }
            else if (bookingStatus == "accepted") {
              this.status = 2;
              getRoute(this.chosen_driver_location, this.dropoff, [{ location: this.pickup, stopover: true }])
                .then(_route => {
                  console.log("Added driver location to map")
                })
                .catch(window.alert);
            }
            else if (bookingStatus == "completed") {
              this.status = 4;
            }
            else if (bookingStatus.includes("delay_")) {
              const delay = bookingStatus.split("_")[1];
              if (delay != this.previousDelay) {
                this.status = 3;
                const splitted = this.arrivalTime.split(" ")
                splitted[splitted.length - 2] = parseInt(splitted[splitted.length - 2]) + (parseInt(delay) - this.previousDelay)
                this.arrivalTime = splitted.join(" ")
                this.previousDelay = delay
              }
            }
            else {
              this.driverMessage = bookingStatus;
            }
          })
          .catch(error => console.log(error));
      },

      onSubmit: function () {
        if (
          this.phone === "" ||
          this.name === "" ||
          this.pickup === "" ||
          this.dropoff === ""
        ) {
          alert("Please fill in the form!");
        } else {
          let pickup_address = this.pickup;
          let dropoff_address = this.dropoff;

          getRoute(pickup_address, dropoff_address)
            .then(route => {
              this.distance = route.distance;
              this.cost = route.cost;
              this.travelTime = route.travelTime;
            })
            .catch(window.alert);
        }
      }
    },
    mounted: function () {
      createMap();
      getCurrentLocation()
        .then(address => {
          this.pickup = address.current_location
        })
    }
  });
}  

//vue for my travels
if ( window.location.pathname.startsWith("/myTravels")){
  new Vue({
    el: "#myTravels",
    data: {
      items: []
    },
    methods: {
      onSubmit: function () { }
    },
    mounted: function () {
      axios
        .get("/api/bookings/user")
        .then(response => {
          //console.log(response)
          this.items = response["data"]["bookings"];
        })
        //.catch(error => console.log(error));
    }
  });
}  

//vue for my travels
if ( window.location.pathname.startsWith("/dMyTravels")){
  new Vue({
    el: "#dMyTravels",
    data: {
      items: []
    },
    methods: {
      onSubmit: function () { }
    },
    created: function () {
      axios
        .get("/api/bookings/driver")
        .then(response => {
          console.log(response)
          this.items = response["data"]["bookings"];
        })
        .catch(error => console.log(error));
    }
  });
}  

if ( window.location.pathname.startsWith("/csrClient")){
  new Vue({
    el: "#csrClient",
    data: {
      type: 0,
      title: "",
      email: "",
      date: "",
      description: "",
      pickup: "",
      dropoff: "",
      status: "unsubmitted"
    },
    methods: {
      onSubmit: function () {
        if (
          this.type === "" ||
          this.title === "" ||
          this.email === "" ||
          this.date === "" ||
          this.description === "" ||
          this.pickup === "" ||
          this.dropoff === ""
        ) {
          alert("Please fill in the form!");
        }
        else {
          this.status = "submitted"
          //api call to save to db
          axios.post("/api/csr/new", {
            csr: {
              type: this.type == 0 ? "lost" : "scam",
              status: "open",
              title: this.title,
              description: this.description,
              dropoff: this.dropoff,
              pickup: this.pickup,
              date: this.date,
              email: this.email
            }
          })
            .then(response => console.log(response))
        }

      }
    },
    mounted: function () { }
  });
}

if ( window.location.pathname.startsWith("/csrOperator")){
  new Vue({
    el: "#csrOperator",
    data: {
      searchBookings: false,
      type: "",
      title: "",
      email: "",
      date: "",
      description: "",
      pickup: "",
      dropoff: "",
      interval: null,
      status: "open",
      reports: [],
      bookings: []
    },
    methods: {
      getBookings: function (bookingDate) {
        this.searchBookings = true
        axios
          .get("/api/bookings?date=" + bookingDate)
          .then(response => {
            //console.log(response)
            this.bookings = response["data"]["info"];
            clearInterval(this.interval);
          })
          //.catch(error => console.log(error));
      },
      resetPage: function () {
        //resets form, updates reports and sets interval
        this.type = "open";
        this.title = "";
        this.email = "";
        this.date = "";
        this.description = "";
        this.pickup = "";
        this.dropoff = "";
        this.status = "open";
        this.searchBookings = false;
        this.updateResults()
        this.interval = window.setInterval(this.updateResults, 15000)
      },
      //updates on change in the form
      updateResults: function () {
        //call to get requests with specified parameters
        axios
          .get("/api/csr?status=open")
          .then(response => {
            //console.log(response)
            this.reports = response["data"]["reports"];
  
          })
          //.catch(error => console.log(error));
      },
  
      onSubmit: function () {
  
        clearInterval(this.interval);
        let parameters = ""
        if (this.pickup !== "") {
          parameters += "&pickup=" + this.pickup
        }
  
        if (this.dropoff !== "") {
          parameters += "&dropoff=" + this.dropoff
        }
  
  
        if (this.type !== "") {
          parameters += "&type=" + this.type
        }
  
        if (this.date !== "") {
          parameters += "&date=" + this.date
        }
  
        axios
          .get("/api/csr?status=" + this.status + parameters)
          .then(response => {
            console.log(response)
            this.reports = response["data"]["reports"];
  
          })
          .catch(error => console.log(error));
      },
  
      changeStatus: function (csrId) {
        //call to change request status
        console.log(this.pickup)
        axios
          .post("/api/csr/update/" + csrId, {
            status: "resolved"
          })
          .then(response => {
            console.log(response)
            this.updateResults()
          })
          .catch(error => console.log(error));
      }
    },
    mounted: function () {
      //call to backend to get all requests with status=open
      this.updateResults()
      this.interval = window.setInterval(this.updateResults, 15000)
  
    }
  });
}

if ( window.location.pathname.startsWith("/driver")){
  new Vue({
    el: "#driver",
    data: {
      driver: -1,
      location: "Eeden Tartu",
      status: "invisible",
      time_late: 0,
      requests: [],
      interval: null,
      current_booking: null,
      clientMessage: ""
    },
    methods: {
      makeAvailable: function () {
        this.status = "available"
        axios
          .post("/api/driver/update/" + this.driver, {
            location: this.location,
            status: this.status
          })
          .then(response => {
            console.log(response)
            this.getDriverBookings()
            
          })
          .catch(error => console.log(error));
      },

      makeInvisible: function () {
        this.status = "invisible"
        axios
          .post("/api/driver/update/" + this.driver, {
            location: this.location,
            status: this.status
          })
          .then(response => {
            console.log(response)
            //this.getDriverBookings()
            //this.interval = window.setInterval(this.getDriverBookings, 15000)
            clearInterval(this.interval);
          })
          .catch(error => console.log(error));
      },

      getDriverBookings: function () {
        axios
          .get("/api/bookings/" + this.driver)
          .then(response => {
            const bookings = response["data"]["bookings"];
            this.requests = bookings;
            console.log("Bookings for driver", this.driver)
            console.log(bookings)
          })
          .catch(error => console.log(error));
      },

      acceptBooking: function (booking) {
        this.current_booking = booking
        let bookingId = booking.id
        console.log(booking);

        axios
          .post("/api/bookings/update/" + bookingId, {
            status: "accepted"
          })
          .then(_ => {
            clearInterval(this.interval);
            this.status = "busy"
            axios
              .post("/api/driver/update/" + this.driver, {
                status: this.status,
                location: this.location
              })
              .then(response => {
                // TODO
              })
          })
      },
      completeBooking: function () {
        axios
          .post("/api/bookings/update/" + this.current_booking.id, {
            status: "completed"
          })
          .then(_ => {
            clearInterval(this.interval);
            this.status = "invisible"
            axios
              .post("/api/driver/update/" + this.driver, {
                status: this.status,
                location: this.current_booking.dropoff
              })
              .then(response => {
                this.current_booking = null;
                this.time_late = 0;
              })
          })
      },
      messageClient: function () {
        let message;
        if (!isNaN(parseInt(this.clientMessage))) {
          this.time_late += parseInt(this.clientMessage)
          message = "delay_" + this.time_late
        }
        else if (this.clientMessage === "") {
          this.time_late += 5;
          message = "delay_" + this.time_late;
        }
        else {
          message = this.clientMessage
        }

        axios
          .post("/api/bookings/update/" + this.current_booking.id, {
            status: message
          })
          .then(_ => {
            console.log("The following message was sent:", message);
          })
      },
      updateDriverDetails: function () {
        axios
          .get("/api/user")
          .then(results => {
            console.log("updated driver details", results.data.user)
            this.location = results.data.user.location,
            this.status = results.data.user.status
            this.driver = results.data.user.id
            this.checkBookingAccepted(results.data.user.id);
          })
      },
      checkBookingAccepted: function (dId) {
        axios
          .get("/api/bookings/accepted/" + dId )
          .then(results => {
            console.log("AcceptedBookings", results)
            this.current_booking = results.data.bookings
          })
      }
    },
    created: function () {
      //getCurrentLocation()
      //.then(address => {
      //this.location = address.current_location
      //})
      this.updateDriverDetails();
      
      this.interval = window.setInterval(this.getDriverBookings, 15000);
    } 
  });
}  

//active links in header
let switchNavMenuItem = (menuItems) => {
  var current = location.pathname
  $.each(menuItems, (index, item) => {
    $(item).removeClass('active')
    if ((current.includes($(item).attr('href')) && $(item).attr('href') !== "/") || ($(item).attr('href') === "/" && current === "/")) {
      $(item).addClass('active');
      $(item).addClass('navActive');
    }

  })
}

$(document).ready(() => {
  switchNavMenuItem($('#nav li a, #nav li link, .nav-item active'))
})

