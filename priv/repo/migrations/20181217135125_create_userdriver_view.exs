defmodule TaxiApp.Repo.Migrations.CreateUserdriverView do
  use Ecto.Migration

  def up do
    execute """
    CREATE VIEW v_user_driver AS
    SELECT u.id as userId, d.id, d.location, d.status
    FROM users u, drivers d
    WHERE d."user_id" = u."id";
    """
  end


  def down do
    execute "DROP VIEW v_user_driver;"
  end  
end
