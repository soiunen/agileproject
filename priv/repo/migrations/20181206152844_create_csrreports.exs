defmodule TaxiApp.Repo.Migrations.CreateCsrreports do
  use Ecto.Migration

  def change do
    create table(:csrreports) do
      add :type, :string
      add :status, :string
      add :title, :string
      add :description, :string
      add :dropoff, :string
      add :pickup, :string
      add :date, :date
      add :email, :string

      timestamps()
    end

  end
end
