defmodule TaxiApp.Repo.Migrations.CreateBookingdriverinfoView do
  use Ecto.Migration

  def up do
    execute """
    CREATE VIEW v_booking_driver_info AS
    SELECT u.name, u.email, u.id as userId, d.id, b."bookingData", b.status, b.cost, b.distance, b.dropoff, b.pickup, b.feedback, b.inserted_at, d.status as driverstatus
    FROM bookings b, drivers d, users u
    WHERE b."driverId" = d."id" and d."user_id" = u."id";
    """
  end


  def down do
    execute "DROP VIEW v_booking_driver_info;"
  end  

end
