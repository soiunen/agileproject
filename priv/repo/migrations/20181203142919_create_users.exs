defmodule TaxiApp.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :password, :string
      add :email, :string
      add :role, :string
      add :balance, :float

      timestamps()
    end

  end
end
