defmodule TaxiApp.Repo.Migrations.CreateBookings do
  use Ecto.Migration

  def change do
    create table(:bookings) do
      add :pickup, :string, null: false
      add :dropoff, :string, null: false
      add :driverId, :integer, null: false, default: 999
      add :clientId, :integer
      add :bookingData, :text
      add :cost, :float
      add :distance, :float
      add :feedback, :text
      add :status, :string

      timestamps()
    end

  end
end
