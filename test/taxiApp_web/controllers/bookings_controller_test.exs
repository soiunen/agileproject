defmodule TaxiAppWeb.BookingsControllerTest do
  use TaxiAppWeb.ConnCase
  alias TaxiApp.{Booking, Repo}

  test "POST /api/bookings/new", %{conn: conn} do

    conn =
      post(conn, "/api/bookings/new", %{
        bookings: [
          pickup: "Liivi 2",
          dropoff: "Lounakeskus",
          bookingData: "Client, number",
          driverId: 1,
          cost: 1.0,
          distance: 1.0,
          status: "open"
        ]
      })

    assert %{"msg" => "API received booking_details message"} = json_response(conn, 201)
  end

  test "GET /api/bookings/:driverId", %{conn: conn} do

    response = get(conn, "/api/bookings/1") |> json_response(200)

    IO.inspect response["bookings"]
    
    assert [%{"bookingData" => "testdata", "clientId" => 10, "cost" => 4.5, "distance" => 5.6, "driverId" => 1, "dropoff" => "Lounakeskus", "feedback" => "Test feedback", "pickup" => "Liivi 2"}] = (response["bookings"])
    
  end

    # test "Booking rejection", %{conn: conn} do
    #   Repo.insert!(%Taxi{status: "busy"})
    #   conn = post conn, "/bookings", %{booking: [pickup_address: "Liivi 2", dropoff_address: "Lõunakeskus"]}
    #   conn = get conn, redirected_to(conn)
    #   assert html_response(conn, 200) =~ ~r/At present, there is no taxi available!/
    # end
end
