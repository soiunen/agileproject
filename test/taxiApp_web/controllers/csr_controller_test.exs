defmodule TaxiAppWeb.CsrControllerTest do
    use TaxiAppWeb.ConnCase
    alias TaxiApp.{Repo, Reports.CSR}

    test "POST /api/csr/new", %{conn: conn} do
  
      conn =
        post(conn, "/api/csr/new", %{
          csr: [
            type: "lost",
            title: "Missing scarf",
            status: "open",
            description: "I left my pink scarf on your taxi yesterday.",
            dropoff: "Liivi 2",
            pickup: "Lounakeskus",
            date: "2018-12-03",
            email: "johnsmith@gmail.com"
          ]
        })

      assert %{"msg" => "CSR report created"} = json_response(conn, 200)
    end

    test "GET /api/csr", %{conn: conn} do
      Repo.insert!(%CSR{date: Ecto.Date.cast!("2018-12-03"), status: "open", email: "john.doe@mail.com", description: "Test", dropoff: "Liivi 2", pickup: "Pikk 2", title: "testtitle", type: "lost"})

      result = get(conn, "/api/csr") |> json_response(200)
      
      assert [
        %{
          "date" => "2018-12-03",
          "description" => "Test",
          "dropoff" => "Liivi 2",
          "email" => "john.doe@mail.com",
          "pickup" => "Pikk 2",
          "status" => "open",
          "title" => "testtitle",
          "type" => "lost"
        }
      ] = result["reports"]
    
    end

  end
  